const commands = {
    "create_project":`
        CREATE ( p:Project {name:$name, _id:$project_id} )
        RETURN p;
    `
}

module.exports = commands