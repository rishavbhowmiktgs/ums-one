var driver = null, neo4j = null;
const config = require('./../config')
const cyps = require('./thing.cyp')     //cyper commands for tag Thing

class Thing{
    //register new Thing
    static async register_thing(tg_id, tg_code){
        if(!driver || !neo4j) throw Error("DB Not Connected")
        const session = driver.session({database: config.DB.NAME,defaultAccessMode: neo4j.session.WRITE})
        const results = await session.run(cyps.register_thing, {tg_id, tg_code})
        await session.close()//always close the session
        return results.records[0]
    }
}

module.exports = ((_driver, _neo4j)=>{
    driver = _driver
    neo4j = _neo4j
    return {Thing}
})