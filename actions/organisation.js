var driver = null, neo4j = null;
const mongodbObjectId = require('mongodb').ObjectId
const config = require('./../config')
const cyps = require('./organisation.cyp')     //cyper commands for tag Organisation

class Organisation{
    static async create_organisation_as_admin(username, password, name){
        if(!driver || !neo4j) throw Error("DB Not Connected")
        const session = driver.session({database: config.DB.NAME,defaultAccessMode: neo4j.session.WRITE})
        const results = await session.run(cyps.create_organisation_as_admin,
            {username, password, name})
        await session.close()//always close the session
        var out = {}
        if(results.records.length && results.records[0].keys.length == 2){
            out[results.records[0].keys[0]] = results.records[0]._fields[0].properties
            out[results.records[0].keys[1]] = results.records[0]._fields[1].properties
        }else
            out = null
        return out
    }
    static async verify_inviter_rights(username, password, org_id){
        if(!driver || !neo4j) throw Error("DB Not Connected")
        const session = driver.session({database: config.DB.NAME,defaultAccessMode: neo4j.session.READ})
        const results = await session.run(cyps.verify_inviter_rights,{username, password, org_id})
        await session.close()//always close the session
        return results.records.length?results.records[0]._fields[0].properties:null
    }
    static async loose_loose_add_member(inviter_username, org_id, username,
        member_type = 'member', things_access_all = false, member_add_all = false){

        if(!driver || !neo4j) throw Error("DB Not Connected")
        const session = driver.session({database: config.DB.NAME,defaultAccessMode: neo4j.session.WRITE})
        const results = await session.run(cyps.loose_loose_add_member,
            {inviter_username, org_id, username, member_type, things_access_all, member_add_all})
        await session.close()//always close the session
        var out = {}
        if(results.records.length && results.records[0].keys.length == 2){
            out[results.records[0].keys[0]] = results.records[0]._fields[0].properties
            out[results.records[0].keys[1]] = results.records[0]._fields[1].properties
        }else
            out = null
        return out
    }
    static async loose_add_member(inviter_username, org_id, username, password,
        member_type = 'member', things_access_all = false, member_add_all = false){
        
        if(!driver || !neo4j) throw Error("DB Not Connected")
        const session = driver.session({database: config.DB.NAME,defaultAccessMode: neo4j.session.WRITE})
        const results = await session.run(cyps.loose_add_member,
            {inviter_username, org_id, username, password, member_type, things_access_all, member_add_all})
        await session.close()//always close the session
        var out = {}
        if(results.records.length && results.records[0].keys.length == 2){
            out[results.records[0].keys[0]] = results.records[0]._fields[0].properties
            out[results.records[0].keys[1]] = results.records[0]._fields[1].properties
        }else
            out = null
        return out
    }
    //subscribe without billing
    static async subscribe_project(username, password, org_id, project_id,
        sub_type='free-subscription', permissions_all = true, permissions_things_all = true){
        
        if(!driver || !neo4j) throw Error("DB Not Connected")
        const session = driver.session({database: config.DB.NAME,defaultAccessMode: neo4j.session.WRITE})
        const results = await session.run(cyps.subscribe_project, 
            {username, password, org_id, project_id, sub_type, permissions_all, permissions_things_all})
        await session.close()//always close the session
        var out = {}
        if(results.records.length && results.records[0].keys.length == 2){
            out[results.records[0].keys[0]] = results.records[0]._fields[0].properties
            out[results.records[0].keys[1]] = results.records[0]._fields[1].properties
        }else
            out = null
        return out
    }

    static async verify_invite_child_right(username, password, org_id){
        if(!driver || !neo4j) throw Error("DB Not Connected")
        const session = driver.session({database: config.DB.NAME,defaultAccessMode: neo4j.session.READ})
        const results = await session.run(cyps.verify_invite_child_right,{username, password, org_id})
        await session.close()//always close the session
        return results.records.length?results.records[0]._fields[0].properties:null
    }

    static async strongloose_add_child_as_admin(iusername, iorg_id, username, password, child_org_id, inherit_resources_all=false, payment_care_all=false){
        if(!driver || !neo4j) throw Error("DB Not Connected")
        const session = driver.session({database: config.DB.NAME,defaultAccessMode: neo4j.session.WRITE})
        const results = await session.run(cyps.strongloose_add_child_as_admin, 
            {iusername, iorg_id, username, password, child_org_id, inherit_resources_all, payment_care_all})
        await session.close()//always close the session
        var out = {}
        if(results.records.length && results.records[0].keys.length == 3){
            out[results.records[0].keys[0]] = results.records[0]._fields[0].properties
            out[results.records[0].keys[1]] = results.records[0]._fields[1].properties
            out[results.records[0].keys[2]] = results.records[0]._fields[2].properties
        }else
            out = null
        return out
    }
}

module.exports = ((_driver, _neo4j)=>{
    driver = _driver
    neo4j = _neo4j
    return {Organisation}
})