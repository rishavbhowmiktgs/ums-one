const mongodbObjectId = require('mongodb').ObjectId
const commands = {
    "create_organisation_as_admin":`
        MATCH (u:User {username:$username, password:$password})
        MERGE (u)<-[mem_on:MEMBER_ON {type:'member'} ]-
        (mr:MemberRights {_id:'${mongodbObjectId()}', type:'admin', things_access_all:true})
        -[mem_of:MEMBER_OF {type:'member'}]->
        (o:Organisation {name:$name, _id:'${mongodbObjectId()}'})
        RETURN mr, o;
    `,
    "verify_inviter_rights":`
        MATCH (u:User {username:$username, password:$password})
        <-[mem_on:MEMBER_ON {type:'member'} ]-
        (mr:MemberRights)
        -[mem_of:MEMBER_OF {type:'member'}]->
        (o:Organisation {_id:$org_id})
        WHERE mr.type = 'admin' OR mr.member_add_all = true
        RETURN o
    `,
    "loose_loose_add_member":`
        MATCH (u:User {username:$username})
        MATCH (iu:User {username:$inviter_username})
        <-[imem_on:MEMBER_ON {type:'member'} ]-
        (imr:MemberRights)
        -[imem_of:MEMBER_OF {type:'member'}]->
        (o:Organisation {_id:$org_id})
        WHERE imr.type = 'admin' OR imr.member_add_all = true
        MERGE (u)<-[mem_on:MEMBER_ON {type:'member'} ]-
        (mr:MemberRights {_id:'${mongodbObjectId()}', type:$member_type, things_access_all:$things_access_all, member_add_all:$member_add_all})
        -[mem_of:MEMBER_OF {type:'member'}]->(o)

        RETURN mr, o
    `,
    "loose_add_member":`
        MATCH (u:User {username:$username, password:$password})
        MATCH (iu:User {username:$inviter_username})
        <-[imem_on:MEMBER_ON {type:'member'} ]-
        (imr:MemberRights)
        -[imem_of:MEMBER_OF {type:'member'}]->
        (o:Organisation {_id:$org_id})
        WHERE imr.type = 'admin' OR imr.member_add_all = true
        MERGE (u)<-[mem_on:MEMBER_ON {type:'member'} ]-
        (mr:MemberRights {_id:'${mongodbObjectId()}', type:$member_type, things_access_all:$things_access_all, member_add_all:$member_add_all})
        -[mem_of:MEMBER_OF {type:'member'}]->(o)

        RETURN mr, o
    `,
    "subscribe_project":`
        MATCH (p:Project {_id:$project_id})
        MATCH (u:User {username:$username, password:$password})
        <-[mem_on:MEMBER_ON {type:'member'} ]-
        (mr:MemberRights)
        -[mem_of:MEMBER_OF {type:'member'}]->
        (o:Organisation {_id:$org_id})
        WHERE mr.type = 'admin'
        MERGE (o)
        -[sub:SUBSCRIPTION {type:$sub_type, permissions_all:$permissions_all, permissions_things_all:$permissions_things_all}]->
        (p)
        RETURN sub, p;
    `,
    "verify_invite_child_right":`
        MATCH (u:User {username:$username, password:$password})
        <-[mem_on:MEMBER_ON {type:'member'} ]-
        (mr:MemberRights)
        -[mem_of:MEMBER_OF {type:'member'}]->
        (o:Organisation {_id:$org_id})
        WHERE mr.type = 'admin'
        RETURN o
    `,
    "strongloose_add_child_as_admin":`
        MATCH (:User {username:$iusername})
        <-[:MEMBER_ON {type:'member'} ]-
        (imr:MemberRights)
        -[:MEMBER_OF {type:'member'}]->
        (po:Organisation {_id:$iorg_id})
        WHERE imr.type = 'admin'
        
        MATCH (:User {username:$username, password:$password})
        <-[:MEMBER_ON {type:'member'} ]-
        (mr:MemberRights)
        -[:MEMBER_OF {type:'member'}]->
        (co:Organisation {_id:$child_org_id})
        WHERE mr.type = 'admin'

        MERGE (po)-[cr:CHILD {inherit_resources_all:$inherit_resources_all, payment_care_all:$payment_care_all}]->(co)

        RETURN po, cr, co
    `

}

module.exports = commands