const commands = {
    "create_user":`
        CREATE ( u:User {username:$username, email:$email, password:$password} )
        RETURN u;
    `,
    "update_user_password_wup":`
        MATCH ( u:User {username:$username, password:$password} )
        SET u.password = $new_password
    `,
    "verify_username_user":`
        MATCH ( u:User {username:$username, password:$password})
        RETURN u;
    `
}

module.exports = commands