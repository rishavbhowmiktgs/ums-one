var driver = null, neo4j = null;
const config = require('./../config')
const cyps = require('./user.cyp')     //cyper commands for tag User

class User{
    //create new user with username, email, password
    static async create_user(username, email, password){
        if(!driver || !neo4j) throw Error("DB Not Connected")
        const session = driver.session({database: config.DB.NAME,defaultAccessMode: neo4j.session.WRITE})
        const results = await session.run(cyps.create_user, {username, email, password})
        await session.close()//always close the session
        return results.records[0]
    }
    static async update_user_password(username, password, new_password){
        if(!driver || !neo4j) throw Error("DB Not Connected")
        const session = driver.session({database: config.DB.NAME,defaultAccessMode: neo4j.session.WRITE})
        const results = await session.run(cyps.update_user_password_wup, {username, password, new_password})
        await session.close()//always close the session
        return results.records
    }
    static async verify_username_user(username, password){
        if(!driver || !neo4j) throw Error("DB Not Connected")
        const session = driver.session({database: config.DB.NAME,defaultAccessMode: neo4j.session.READ})
        const results = await session.run(cyps.verify_username_user, {username, password})
        await session.close()//always close the session
        var out = {}
        if(results.records.length && results.records[0].keys.length == 1){
            out[results.records[0].keys[0]] = results.records[0]._fields[0].properties
        }else
            out = null
        return out
    }
}

module.exports = ((_driver, _neo4j)=>{
    driver = _driver
    neo4j = _neo4j
    return {User}
})