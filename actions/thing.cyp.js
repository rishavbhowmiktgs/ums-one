const commands = {
    "register_thing":`
        CREATE ( tg:Thing {tg_id:$tg_id, tg_code:$tg_code} )
        RETURN tg;
    `
}

module.exports = commands