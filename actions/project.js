var driver = null, neo4j = null;
const mongodbObjectId = require('mongodb').ObjectId
const config = require('./../config')
const cyps = require('./project.cyp')     //cyper commands for tag Project

class Project{
    static async create_project(name){
        if(!driver || !neo4j) throw Error("DB Not Connected")
        const project_id = `${mongodbObjectId()}`
        const session = driver.session({database: config.DB.NAME,defaultAccessMode: neo4j.session.WRITE})
        const results = await session.run(cyps.create_project, {name, project_id})
        await session.close()//always close the session
        return results.records
    }
}

module.exports = ((_driver, _neo4j)=>{
    driver = _driver
    neo4j = _neo4j
    return {Project}
})