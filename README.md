## Recursive Query
```cypher
MATCH (john:Person { name:"John" })-[:friend *1..2]->(friend: Person)
RETURN friend.name, friend.age;
To go n nevel deep, don't put anything i.e. *1..
```

- So what does *1..2 here means:

- `*` to denote that its a recursion.

- `1` to denote that do not include john itself i.e. is the start node. If you put 0 here, it will also include the John node itself.

.. to denote that go from this node till ...

2 denotes the level of recursion. Here you say to stop at level 2. i.e. dont go beyond steve. If you put nothing there, it will keep on going until it cannot find a node that "has" a friend relationship