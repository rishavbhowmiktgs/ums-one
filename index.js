var neo4j = require('neo4j-driver')
const private_config = require('../private/private_config.json')

var driver = neo4j.driver(
    'neo4j://localhost',
    neo4j.auth.basic(private_config.NEO_USERNAME, private_config.NEO_PASSWORD)
  )

const user_actions = require('./actions/user')(driver, neo4j)
const thing_actions = require('./actions/thing')(driver, neo4j)
const project_actions = require('./actions/project')(driver, neo4j)
const organisation_actions = require('./actions/organisation')(driver, neo4j);

//unit tests
(async ()=>{
  /*await user_actions.User.create_user("u6", "u6@u.com", "pass")
  //.then((ff)=>{console.log(ff)})
  //.catch((rs)=>{console.error(rs)})
  console.log("19");

  await user_actions.User.update_user_password("u6", "pass", "pass")
  .then((ff)=>{console.log(ff);console.log(Date.now());})
  .catch((rs)=>{console.error(rs)})
  console.log("23");
  /*
  await thing_actions.Thing.register_thing("TG0104", "8U7YPT5R")
  //.then((ff)=>{console.log(ff);})
  //.catch((rs)=>{console.error(rs)})
  console.log("27");*/
  
  //const proj = await project_actions.Project.create_project('don3')
  //.then((ff)=>{console.log(ff);})
  //.catch((rs)=>{console.error(rs)})
  //console.log("31", proj[0]._fields[0].properties._id);

  //const org = await organisation_actions.Organisation.create_organisation_as_admin("u6", "pass", "org12")
  //.then((ff)=>{console.log(ff);})
  //.catch((rs)=>{console.error(rs)})
  //console.log("Done", org.o);
/*
  await organisation_actions.Organisation.verify_inviter_rights("u6", "pass", "")
  .then((ff)=>{console.log(ff);})
  .catch((rs)=>{console.error(rs)})

  await organisation_actions.Organisation.loose_loose_add_member("u6", "", "u5")
  .then((ff)=>{console.log(ff);})
  .catch((rs)=>{console.error(rs)})
  console.log("org", org);

  await organisation_actions.Organisation.subscribe_project("u6", "pass", org.o._id, '')
  .then((ff)=>{console.log(ff);})
  .catch((rs)=>{console.error(rs)})
*/
  /*await organisation_actions.Organisation.verify_invite_child_right
  ("u6", "pass", "")
  .then((ff)=>{console.log(ff);})
  .catch((rs)=>{console.error(rs)})

  organisation_actions.Organisation.strongloose_add_child_as_admin("u6", "", "u6", "pass", "")
  .then((ff)=>{console.log(ff);})
  .catch((rs)=>{console.error(rs)})*/

  user_actions.User.verify_username_user("u6", "pass")
  .then((ff)=>{console.log(ff);})
  .catch((rs)=>{console.error(rs)})
})()

module.exports = {
  user_actions,
  thing_actions,
  project_actions,
  organisation_actions
}