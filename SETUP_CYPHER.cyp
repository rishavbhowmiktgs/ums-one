CREATE DATABASE ums IF NOT EXISTS;

USE ums;

CREATE CONSTRAINT one_username IF NOT EXISTS
ON (u:User) ASSERT u.username IS UNIQUE;
CREATE CONSTRAINT one_email IF NOT EXISTS
ON (u:User) ASSERT u.email IS UNIQUE;

CREATE CONSTRAINT one_project_name IF NOT EXISTS
ON (p:Project) ASSERT p.name IS UNIQUE;
CREATE CONSTRAINT one_project_id IF NOT EXISTS
ON (p:Project) ASSERT p._id IS UNIQUE;

CREATE CONSTRAINT one_memberrights_id IF NOT EXISTS
ON (mr:MemberRights) ASSERT mr._id IS UNIQUE;

CREATE CONSTRAINT one_organisation_name IF NOT EXISTS
ON (o:Organisation) ASSERT o.name IS UNIQUE;
CREATE CONSTRAINT one_organisation_id IF NOT EXISTS
ON (o:Organisation) ASSERT o._id IS UNIQUE;

CREATE CONSTRAINT one_tg_id IF NOT EXISTS
ON (tg:Thing) ASSERT tg.tg_id IS UNIQUE;